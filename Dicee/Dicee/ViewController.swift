//
//  ViewController.swift
//  Dicee
//
//  Created by Jesse Frederick on 1/31/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var diceImage1: UIImageView!
    @IBOutlet weak var diceImage2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func rollButton(_ sender: UIButton) {
        
        let diceArray = [UIImage(imageLiteralResourceName: "dice1"), UIImage(imageLiteralResourceName: "dice2"), UIImage(imageLiteralResourceName: "dice3"), UIImage(imageLiteralResourceName: "dice4"), UIImage(imageLiteralResourceName: "dice5"), UIImage(imageLiteralResourceName: "dice6")]
        
        diceImage1.image = diceArray[Int.random(in: 0...5)]
        diceImage2.image = diceArray[Int.random(in: 0...5)]
    }
}

